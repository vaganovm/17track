<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.10.16
 * Time: 20:59
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

$pids = array_filter(explode("\n", shell_exec('ps -A x | grep "run17Track.ph[p]"')));
foreach ($pids as &$pid) {
    $pid = (int)$pid;
}
if (count($pids) > 2) {
    die();
}

chdir(dirname(__DIR__));
set_include_path(dirname(dirname(__FILE__)) . "/");
$dir_site = dirname(dirname(__FILE__));
include('init_autoloader.php');
$application = Zend\Mvc\Application::init(
    require_once 'config/application.config.php'
);
try {
    while (true){

        $lastNumber = file_get_contents('/tmp/17track/lastNumber.txt');
        $dataNumber = [];
        $getParams = [];
        for ($i = 0; $i < 30; $i++) {
            $lastNumber = (int)$lastNumber + 1;
            $num = 'RA' . $lastNumber . getContralNumber($lastNumber) . 'FI';
            $dataNumber[]['num'] = $num;
            $getParams[] = $num;
        }
        $getParams = implode(',', $getParams);
        $request = [
            'guid' => "",
            'data' => $dataNumber,
        ];
        $request = json_encode($request);
	echo $request . "\n";

    //$str = '{"guid":"","data":[{"num":"RA202184466FI"},{"num":"RA202393393FI"}]}';
    //$jsonTest = json_decode($str);
        $urlGet = 'http://www.17track.net/ru/track?nums='.$getParams.'&fc=0';
        echo $urlGet . "\n";



        $client = new \Zend\Http\Client();

        $cookies = new Zend\Http\Cookies();

// First request: log in and start a session
        $client->setUri($urlGet);
        $client->setMethod('GET');

        $response = $client->send();
        $cookies->addCookiesFromResponse($response, $client->getUri());
        /** @var \Zend\Http\Header\SetCookie $d */
        $d = end($cookies->getAllCookies());
        $userid = $d->getValue();

       // sleep(10);



// Now we can send our next request
        $client->setUri('http://www.17track.net/restapi/handlertrack.ashx');
       // $client->addCookie($cookies->getMatchingCookies($client->getUri()));
        $client->setHeaders(
            [


                'Accept' =>  '*/*',
'Accept-Encoding' => 'gzip, deflate',
'Accept-Language' => 'en-US,en;q=0.5',
'Cache-Control' => 'max-age=10',

'Connection' => 'keep-alive',
'Content-Length'=>strlen($request),
'Content-Type' =>'application/x-www-form-urlencoded; charset=UTF-8',
                'Cookie' =>'__cfduid=' . $userid,
'DNT'=>1,
                'Host' => 'www.17track.net',
'Referer' =>$urlGet,

'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0',
'X-Requested-With' => 'XMLHttpRequest',
        ]
        );
        $client->setRawBody('{"guid":"","data":[{"num":"RA202451754FI"}]}');

        $result = httpPost('http://www.17track.net/restapi/handlertrack.ashx',$request, '');
        sleep(5);
        $result = httpPost('http://www.17track.net/restapi/handlertrack.ashx',$request, '');
        $client->setMethod('POST');
//       $result = $client->send();
//        $result = $result->getBody();
//        sleep(60);
//        $result = $client->send();
//        $result = $result->getBody();
//        $client->clearCookies();







	var_dump($result);
        $sort = [];
        $fileSort = '/tmp/17track/';
        $searchNeed = [
            'Orenburg',
            'Ekaterinburg'
        ];
        if ($result) {
            $result = json_decode($result, true);
            $sucess = 0;
            foreach ($result['dat'] as $row) {
                $sucess = 1;
                $searchStr = serialize($row);
                foreach ($searchNeed as $rowSearch) {
                    if (strpos($searchStr, $rowSearch) !== false) {
                        $sort[$rowSearch] .= $row['no'];
                        if (array_key_exists('z0', $row['track'])) {
                            $tmp = [
                                $row['track']['z0']['a'],
                                $row['track']['z0']['c'],
                                $row['track']['z0']['z'],
                            ];
                            $sort[$rowSearch] .= '|' . implode('|', $tmp) . "\n";
                        }
                    }
                }
            }
            if (!empty($sort)) {
                foreach ($sort as $filePostFix => $row) {
                    $filePath = $fileSort . $filePostFix . '.txt';
                    $result = file_put_contents($filePath, $row, FILE_APPEND);
                }
            }
            if ($sucess){
                file_put_contents('/tmp/17track/lastNumber.txt', (string)$lastNumber);
            }
        } else {
            echo 'Результат NULL ' . date('Y.m.d H:i:s') . "\n";
        }
        sleep(rand(60, 120));
    }
}

catch (Exception $e){
    echo $e->getMessage();
}

function httpPost($url, $data, $reffer)
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}

function getContralNumber($num)
{
    $num = (string)$num;
    $mul = [null,8, 6, 4, 2, 3, 5, 9, 7];
    $sum = 0;
    for ($i = 0; $i < 8; $i++){



        $sum += ($num[$i]) * (next($mul));
    }
    return 11 - ($sum%11);
}
